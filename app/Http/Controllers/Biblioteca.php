<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Livros;

use App\Models\Editoras;

use App\Models\Autores;

class Biblioteca extends Controller
{
    public function retorno(){
        $livro = Livros::all();
        return view('listagemLivros',['livro'=>$livro]);
    }

    public function autor(){
        $autor = Autores::all();
        return view('listagemAutores',['autor'=>$autor]);
    }

    public function editora(){
        $editora = Editoras::all();
        return view('listagemEditoras', ['editora'=>$editora]);
    }

    public function editautor(){
        $autor = Autores::all();
        return view('editarAutores',['autor'=>$autor]);
    }

    public function editlivro(){
        $livro = Livros::all();
        return view('editarLivros', ['livro'=>$livro]);
    }

    public function editeditora(){
        $editora = Editoras::all();
        return view('editarEditoras', ['editora'=>$editora]);
    }
}