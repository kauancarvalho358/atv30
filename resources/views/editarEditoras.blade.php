<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Biblioteca</title>
    </head>
    <body>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/">Home</a></li>
                <li class="item"><a href="/biblioteca">Livros</a></li>
                <li class="item"><a href="/autores">Autores</a></li>
                <li class="item"><a href="/editora">Editoras</a></li>
            </ul>
        </nav>
        <nav>
            <ul style = "text-align:center" class="menu">
                <li class="item"><a href="/editarlivros">Editar livros</a></li>
                <li class="item"><a href="/editarautores">Editar autores</a></li>
                <li class="item"><a href="/editareditoras">Editar editoras</a></li>  
            </ul>
        </nav>
        <hr color="black">
        <div border='4'>
            <h1  style = "text-align:center">Listagem das Editoras</h1>
            <table border='1' width="500" align="center">
                <tr>
                    <td  class="tab1" width="500">
                        <h4>ID da Editora:</h4>
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Nome do Editora:</h4>
                    </td>
                    <td  class="tab1" width="500">
                        <h4>Ações</h4>
                    </td>
                </tr>
                @foreach($editora as $editora)
                <tr>
                    <td  class="tab" width="500">
                        {{$editora->id}}
                    </td>
                    <td  class="tab" width="500">
                        {{$editora->editora}}
                    </td>
                    <td style = "text-align:center" class="tab" width="500">
                        <button id="btneditar">Editar</button> <button id="btndeletar">Deletar</button>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </body>
</html>